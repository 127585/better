// Initialize variables
let score = 0;

// Initialize AWS
AWS.config.region = "us-east-2";
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: "us-east-2_5TLEkiLHO"
});

// Get a reference to the Amazon DynamoDB service
const ddb = new AWS.DynamoDB.DocumentClient();

// Update the leaderboard
function updateLeaderboard() {
  // Clear the leaderboard
  document.getElementById("leaderboard").innerHTML = "";

  // Query the leaderboard table
  const params = {
    TableName: "leaderboard",
    KeyConditionExpression: "pk = :pk",
    ExpressionAttributeValues: {
      ":pk": "leaderboard"
    },
    ScanIndexForward: false,
    Limit: 10
  };
  ddb.query(params, function(err, data) {
    if (err) {
      console.log("Error getting leaderboard: ", err);
    } else {
      // Loop through the items and add them to the leaderboard
      data.Items.forEach(function(item) {
        const li = document.createElement("li");
        li.textContent = item.name + ": " + item.score;
        document.getElementById("leaderboard").appendChild(li);
      });
    }
  });
}

// Update the score
function updateScore() {
  // Update the score
  score++;
  document.getElementById("score").textContent = score;

  // Update the leaderboard
  updateLeaderboard();

  // Update the leaderboard table
  const params = {
    TableName: "leaderboard",
    Item: {
      pk: "leaderboard",
      sk: new Date().toISOString(),
      name: "Player " + Math.floor(Math.random() * 10000),
      score: score
    }
  };
  ddb.put(params, function(err, data) {
    if (err) {
      console.log("Error updating leaderboard: ", err);
    }
  });
}

// Add a click event listener to the button
document.getElementById("clicker").addEventListener("click", function() {
  updateScore();
});
